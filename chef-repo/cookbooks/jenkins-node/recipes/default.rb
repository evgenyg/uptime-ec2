#
# Author:: Evgeny Goldin (<evgenyg@gmail.com>)
# Cookbook Name:: jenkins-node
# Recipe:: default
# Copyright 2013, Evgeny Goldin
# Licensed under the Apache License, Version 2.0 (the "License");
#

setup_keys
update_system
setup_groovy
setup_mongodb
add_jenkins_node
setup_sudoers
