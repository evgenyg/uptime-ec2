
##
# Creates SSH public and private keys.
#
def setup_keys
  keys_data = Chef::EncryptedDataBagItem.load( 'keys', 'uptime', node[:encrypted_data_bag_secret] )
  mkfile( "#{node[:home]}/.ssh/id_dsa",      keys_data['private'], 0600 )
  mkfile( "#{node[:home]}/.ssh/id_dsa.pub",  keys_data['public'],  0600 )
  mkfile( "#{node[:home]}/.ssh/known_hosts", 'known_hosts',        0600, false )
end


##
# Updates the system, sets up .bash_profile and hostname.
#
def update_system
  Execute 'apt-get update'
  Execute 'apt-get upgrade --yes --show-upgraded'
  'tar gzip unzip curl wget htop git'.split.each { |p| Package p }

  Template "#{ node[:home] }/.bash_profile" do
    source 'bash_profile.erb'
    owner  node[:user]
    group  node[:group]
    mode   0644
    variables({ :java_home   => node[:java][:home],
                :groovy_home => node[:groovy][:home] })
  end

  File    '/etc/hostname' do content Chef::Config[:node_name] end
  Service 'hostname'      do action :restart end
end


##
# Sets up Groovy.
# https://github.com/RiotGames/groovy-cookbook
#
def setup_groovy
  include_recipe 'groovy'
end


##
# Sets up MongoDB.
#
def setup_mongodb
  Bash "install_mongodb" do
    user node[:user]
    # http://docs.mongodb.org/manual/tutorial/install-mongodb-on-debian/
    code <<-EOH
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10
echo 'deb http://downloads-distro.mongodb.org/repo/debian-sysvinit dist 10gen' | sudo tee /etc/apt/sources.list.d/10gen.list
sudo apt-get update
sudo apt-get install mongodb-10gen=2.2.3 -y --force-yes
echo "mongodb-10gen hold" | sudo dpkg --set-selections
sudo mkdir -p /data/db
sudo chown -R #{node[:user]}:#{node[:group]} /data/db
mongo  --version
mongod --version
    EOH
  end
end


##
# 'Directory' resource wrapper.
#
def mkdir( dir, mode = 0744 )
  Directory dir do
    owner     node[:user]
    group     node[:group]
    mode      mode
    recursive recursive
    action    :create
  end
end


##
# Sets up '/etc/sudoers'.
#
def setup_sudoers
  Template '/etc/sudoers' do
    source 'sudoers.erb'
    owner  'root'
    group  'root'
    mode   0440
  end
end


##
# 'File' resource wrapper.
#
def mkfile( location, data, mode = 0644, is_file = true )
  if is_file then
    File location do
      content data
      owner   node[:user]
      group   node[:group]
      mode    mode
      action  :create
    end
  else
    Cookbook_File location do
      source  data
      owner   node[:user]
      group   node[:group]
      mode    mode
      action  :create
    end
  end
end


