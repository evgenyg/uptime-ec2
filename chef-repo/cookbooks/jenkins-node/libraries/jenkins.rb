require 'open-uri'

##
# Creates new Jenkins node and adds it to the master instance.
#
def add_jenkins_node

  jenkins_home = node[:jenkins][:home]

  unless File.directory?( jenkins_home )

    node_name    = Chef::Config[:node_name]
    jenkins_data = Chef::EncryptedDataBagItem.load( 'jenkins', 'user', node[:encrypted_data_bag_secret] )

    mkdir ( jenkins_home )

    Template "#{jenkins_home}/addNode.groovy" do
      source 'addNode.groovy.erb'
      owner  node[:user]
      group  node[:group]
      mode   0644
      variables({ :name        => node_name,
                  :description => "EC2 node - \"#{node_name}\"",
                  :home        => jenkins_home,
                  :executors   => 2,
                  :label       => node_name,
                  :port        => 22,
                  :user        => node[:user],
# AWS *.pem key path on master Jenkins (copied manually). Create an entry for it in "Manage Credentials" (http://jenkins/credentials/):
# "From a file on Jenkins master" = same path, see https://www.dropbox.com/s/hufabrwatls29kv/Credentials.png
                  :key_path    => '/home/evgenyg/.ssh/ec2.pem' })
    end

    Template '/etc/init.d/add_jenkins_node' do
      owner   'root'
      group   'root'
      mode    '0744'
      variables({ :jenkins_home => jenkins_home,
                  :jenkins_url  => node[:jenkins][:url],
                  :java_home    => node[:java][:home],
                  :user         => jenkins_data['user'],
                  :password     => jenkins_data['password'] })
    end

    Bash 'add_jenkins_node' do
      user node[:user]
      cwd jenkins_home
      code <<-END
sudo update-rc.d add_jenkins_node defaults 90
sudo /etc/init.d/add_jenkins_node
      END
    end
  end
end
