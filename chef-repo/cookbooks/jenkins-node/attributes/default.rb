
default[:user]               = 'ubuntu'
default[:group]              = 'ubuntu'
default[:home]               = "/home/#{ default[:user] }"

default[:java][:home]        = '/usr/lib/jvm/default-java'
default[:jenkins][:url]      = 'http://evgeny-goldin.org/jenkins'
default[:jenkins][:home]     = "#{ default[:home] }/.jenkins"

node.set[:groovy][:home]     = '/usr/lib/groovy'
node.set[:groovy][:version]  = '2.1.4'
node.set[:groovy][:url]      = "http://evgenyg.artifactoryonline.com/evgenyg/ext-releases-local/groovy-binary-#{ node[:groovy][:version] }-min.zip"
node.set[:groovy][:checksum] = '1c4410ac49b0313bccf0f596b19bb9a3894550a3044553cfc0a8cc6c73d0ca7c'
