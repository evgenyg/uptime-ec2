name              "jenkins-node"
maintainer        "Evgeny Goldin"
maintainer_email  "evgenyg@gmail.com"
license           "Apache 2.0"
description       "Installs Jenkins Node."
long_description  IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version           "0.0.1"

depends           'groovy'
