#
# Cookbook Name:: groovy
# Attributes:: default
#
# Copyright (C) 2012 Kyle Allan (<kallan@riotgames.com>)
#
# All rights reserved - Do Not Redistribute
#
default[:groovy][:version]    = '2.0.1'
default[:groovy][:home]       = '/usr/local/groovy'
default[:groovy][:url]        = 'http://dist.groovy.codehaus.org/distributions/groovy-binary-2.0.1.zip'
default[:groovy][:checksum]   = 'c8379e19da3f672036d0a9f7383fe7ba143276066077bff29d50c6cbab3f8c0b'