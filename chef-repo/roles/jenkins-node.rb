name        'jenkins-node'
description 'Jenkins Node'

override_attributes(
    # https://github.com/opscode-cookbooks/java
    # http://www.oracle.com/technetwork/java/javase/downloads/index.html
    'java' => {
        'oracle'         => { 'accept_oracle_download_terms' => true },
        'jdk_version'    => 7,
        'install_flavor' => 'oracle',
        'jdk'            => { '7' => { 'x86_64' => { 'url' => 'http://download.oracle.com/otn-pub/java/jdk/7u21-b11/jdk-7u21-linux-x64.tar.gz' }}}
    }
)

env_run_lists '_default' => [ 'recipe[java::oracle]', 'recipe[jenkins-node]' ]
