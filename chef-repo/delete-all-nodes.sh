#!/bin/bash

set -e
set -o pipefail
set +x

for instance_id in `knife ec2 server list | grep -v "Instance ID" | awk '{print $1}'`; do
    echo ">> knife ec2 server delete $instance_id"
    knife ec2 server delete $instance_id --yes
done

for node_name in `knife node list | awk '{print $1}'`; do
    echo ">> knife node delete $node_name"
    knife node delete $node_name --yes
    echo ">> knife client delete $node_name"
    knife client delete $node_name --yes
done

./list.sh
