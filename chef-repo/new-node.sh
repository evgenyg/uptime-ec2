#!/bin/bash

set -e
set -o pipefail
set -x

# http://docs.opscode.com/plugin_knife_ec2.html
# https://github.com/opscode/knife-ec2

# Arguments:
# $1 - Node name
# $2 - EC2 instance (t1.micro, m1.small - https://aws.amazon.com/ec2/instance-types/#instance-details)

ls -al "/Volumes/NO NAME/Projects/encrypted_data_bag_secret"
knife ec2 server create -i ~/.ssh/ec2.pem -N $1 -f $2 -I ami-640a0610 -x ubuntu -g default -r "role[jenkins-node]"
./list.sh
