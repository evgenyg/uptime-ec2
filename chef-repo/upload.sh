#!/bin/bash

set -e
set -o pipefail
set -x

foodcritic -C cookbooks/jenkins-node
knife cookbook upload -o cookbooks jenkins-node
knife role from file roles/jenkins-node.rb
