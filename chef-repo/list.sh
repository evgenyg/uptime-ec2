#!/bin/bash

set -e
set -o pipefail
set -x

knife ec2 server list
knife node list
knife client list
