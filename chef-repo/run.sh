#!/bin/bash

set -e
set -o pipefail
set -x

./delete-all-nodes.sh && ./upload.sh && ./new-node.sh uptime-ec2 m1.small
