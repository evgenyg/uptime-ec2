#!/bin/bash

set +e
set -o pipefail
set -x

# http://docs.opscode.com/plugin_knife_ec2.html
# https://github.com/opscode/knife-ec2

# Arguments (get them from running ./list.sh):
# $1 - Instance ID
# $2 - Node name

knife ec2 server delete $1 --yes
knife node       delete $2 --yes
knife client     delete $2 --yes

set -e

./list.sh
