uptime-ec2
==========

A remote monitoring application using Node.js, MongoDB, and Twitter Bootstrap, adapted to run on AWS EC2 using Chef cookbooks and [Gradle Node.js plugin](http://evgeny-goldin.com/wiki/Gradle-node-plugin).

Project Structure
-----------------

* `"uptime"` - original application, forked and adapted from **[github.com/fzaninotto/uptime](http://github.com/fzaninotto/uptime)**.
* `"chef-repo"` - Chef roles and cookbooks required to set up an AWS EC2 box as Jenkins slave node.
	* `"*.sh"` - Bash scripts to create, list and delete AWS EC2 instances and Chef nodes.
	* `"roles/jenkins-node.rb"` + `"cookbooks/jenkins-node"` - Chef role and cookbook to be applied to newly created EC2 nodes.
	* Run `"./upload.sh"` to upload `"jenkins-node"` role and cookbook to remote Chef server. Note: it uses a [foodcritic](http://acrmp.github.io/foodcritic/) (`gem install foodcritic`).
* `"jenkins"` - Jenkins jobs configurations.

Chef Server
-----------

This project assumes a usage of a Chef Server so you should have `"~/.chef/knife.rb"`, similar to this one:

    log_level                :info
    log_location             STDOUT
    node_name                "..."
    validation_client_name   "..."
    client_key               "#{current_dir}/key.pem"
    validation_key           "#{current_dir}/key-validator.pem"
    chef_server_url          "https://api.opscode.com/organizations/..."
    cache_type               'BasicFile'
    cache_options( :path => "#{ENV['HOME']}/.chef/checksums" )
    cookbook_path            '...'
    current_dir                   = File.dirname(__FILE__)
    knife[:aws_ssh_key_id]        = '...'
    knife[:availability_zone]     = '...'
    knife[:region]                = '...'
    knife[:aws_access_key_id]     = '...'
    knife[:aws_secret_access_key] = '...'
    encrypted_data_bag_secret       '.../encrypted_data_bag_secret'

Jenkins Authentication
----------------------

Jenkins master instance URL is specifed in `"chef-repo/cookbooks/jenkins-node/attributes/default.rb"`. Admin credentials are stored in encrypted `"jenkins" => "user"` data bag:

    > knife data bag show jenkins user --secret-file ".../encrypted_data_bag_secret"
    id:        user
    password:  adminPassword
    user:      adminUsername
    >

Running Uptime Locally
-----------------------

    > ./gradlew -i help
    > ./gradlew -i start
    > ./gradlew -i stop


Running Uptime on EC2
----------------------

    > cd chef-repo
    > ./new-node.sh uptime-ec2 m1.small

This creates `"uptime-ec2"` Jenkins node slave. Now define Jenkins jobs using configurations stored in `"jenkins/jobs"` and run `"Uptime Run"` or `"Uptime Run (Node.js Version)"` job.


Support
-------

Feel free to contact me (**evgenyg@gmail.com**) if you experience any problems with running this project. I have it running perfectly fine so I'll be glad to help you out with any issue.

